/*
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
Події це механізм, що дозволяє реагувати на дії користувача, вони використовуються для запуску функцій.
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
click, dblclick, mouseover, mouseout, mousemove
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
ця подія вимикає стандартне контекстне меню браузера при натискані правою клавішею на елемент.

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
  */

const btn = document.querySelector("#btn-click");
const section = document.querySelector("#content");

btn.addEventListener("click", (e) => {
    const p = document.createElement("p");
    p.textContent = "New Paragraph";
    section.appendChild(p);
});
/* 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
*/

const btnCreate = document.createElement("button");
btnCreate.setAttribute("id", "btn-input-create");
btnCreate.innerHTML = "Create input";
btnCreate.addEventListener("click", (e) => {
    const input = document.createElement("input");
    input.type = 'text';
    input.placeholder = "23";
    input.name = "input";
    section.append(input);

})
section.append(btnCreate);